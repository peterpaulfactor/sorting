# # Number Search

def binary_search(sequence, item):
    begin_index = 0
    end_index = len(sequence) - 1

    while begin_index <= end_index:
        midpoint = begin_index + (end_index - begin_index) // 2
        midpoint_value = sequence[midpoint]
        if midpoint_value == item:
            return midpoint

        elif item < midpoint_value:
            end_index = midpoint - 1

        else:
            begin_index = midpoint + 1

    return(print("The entered number is not in the list."))

def selection_sort(sequence):
    indexing_length = range(0, len(sequence)-1)

    for i in indexing_length:
        min_value = i

        for j in range(i+1, len(sequence)):
            if sequence[j] < sequence[min_value]:
                min_value = j

        if min_value != i:
            sequence[min_value], sequence[i] = sequence[i], sequence[min_value]

    return sequence

print(" ")

sequence = (selection_sort([int(sequence) for sequence in input("Enter a list of numbers: ").split(',')]))

print("The numbers are: ", sequence)

item_a = int(input("What is the target number: "))

print("The target number is in index: ", binary_search(sequence, item_a))

