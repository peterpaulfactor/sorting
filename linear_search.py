# Numbers Search

def linear_search(index, n, x):

    for i in range(0, n):
        if(index[i] == x):
            return i
    return -1
 
print(" ")

index = [int(index) for index in input("Enter a list of numbers: ").split(',')]

print("The numbers are: ", index)

x = int(input("What is the target number: "))
n = len(index)

result = linear_search(index, n, x)

print("The count of the given numbers is: ", n)

if(result == -1):
    print("The give target is not found") 
else:
    print("I found it!")
    print("The number is in index: ", result) 



# ________________________________________________________________________________________________________________________________________________________________________



# Names Search

def linear_search(indexx, nn, xx):

    for i in range(0, nn):
        if(indexx[i] == xx):
            return i
    return -1

print(" ")

indexx = [str(indexx) for indexx in input("Enter a list of names: ").split(',')]

print("The names are: ", indexx)

xx = str(input("Give the target name: "))
nn = len(indexx)

resultt = linear_search(indexx, nn, xx)

print("The count of the given names is: ", nn)

if(resultt == -1):
    print("The give target is not found")  
else:
    print("I found", xx) 
    print("The name is in index: ", resultt) 
    


# ________________________________________________________________________________________________________________________________________________________________________
