def selection_sort(n_sequence):
    indexing_length = range(0, len(n_sequence)-1)

    for i in indexing_length:
        min_value = i

        for j in range(i+1, len(n_sequence)):
            if n_sequence[j] < n_sequence[min_value]:
                min_value = j

        if min_value != i:
            n_sequence[min_value], n_sequence[i] = n_sequence[i], n_sequence[min_value]

    return n_sequence

print(' ')

n_sequence = (selection_sort([int(n_sequence) for n_sequence in input("Enter a list of numbers: ").split(',')]))

print("The sorted numbers are: ", n_sequence)