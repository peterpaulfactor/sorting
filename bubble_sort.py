
def bubble(number_list):
    indexing_length = len(number_list) - 1
    sorted = False

    while not sorted:
        sorted = True
        for i in range(0, indexing_length):
            if number_list[i] > number_list[i+1]:
                sorted = False
                number_list[i], number_list[i+1] = number_list[i+1], number_list[i]
    return number_list

print(' ')

number_list = (bubble([int(number_list) for number_list in input("Enter a list of numbers: ").split(',')]))

print("The sorted numbers are:", number_list)